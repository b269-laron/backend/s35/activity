const express = require("express");
const  mongoose = require("mongoose");

const app = express();
const port = 3001;

// Connecting to MongoDB Atlas - Start

mongoose.connect("mongodb+srv://laronjhon:admin123@zuitt-bootcamp.tyql6ow.mongodb.net/s35?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	}	
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get("/signup", (req, res) => {
	User.find({}).then((result, err) => {
		if(err) {
			return console.log(err)
		} else {
			return res.status(200).json({
				data: result // we can also use "result" only
			})
		}
	})
})

const userSchema = new mongoose.Schema({
	name: String,
	password: String
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
	User.findOne({name: req.body.username}).then((result, err) => {

		if (result != null && result.username == req.body.username){
			return res.send("Duplicate user found");
		} else {
			let newUser = new User ({
				name: req.body.username,
				password: req.body.password
			});
			newUser.save().then((savedUser, saveErr) => 
			{
				if (saveErr){
					return console.log(saveErr);
				} else {
					return res.status(201).send("New user created!");
				}
			})
		}
	})	
});

app.get("/signup", (req, res) => {
	User.find({}).then((result, err) => {
		if(err) {
			return console.log(err)
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port}`));